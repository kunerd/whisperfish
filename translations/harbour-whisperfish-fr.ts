<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name></name>
    <message id="whisperfish-cover-new-label">
        <location filename="../qml/cover/UnreadLabel.qml" line="25"/>
        <source>New</source>
        <extracomment>Cover new message label</extracomment>
        <translation>Nouveau</translation>
    </message>
    <message id="whisperfish-session-has-attachment">
        <location filename="../qml/delegates/Session.qml" line="69"/>
        <source>Attachment</source>
        <extracomment>Session contains an attachment label</extracomment>
        <translation>Pièce jointe</translation>
    </message>
    <message id="whisperfish-session-delete-all">
        <location filename="../qml/delegates/Session.qml" line="117"/>
        <source>Deleting all messages</source>
        <extracomment>Delete all messages from session</extracomment>
        <translation>Suppression de tous les messages</translation>
    </message>
    <message id="whisperfish-delete-session">
        <location filename="../qml/delegates/Session.qml" line="132"/>
        <source>Delete Conversation</source>
        <extracomment>Delete all messages from session menu</extracomment>
        <translation>Supprimer la conversation</translation>
    </message>
    <message id="whisperfish-notification-default-message">
        <location filename="../qml/harbour-whisperfish.qml" line="41"/>
        <source>New Message</source>
        <extracomment>Default label for new message notification</extracomment>
        <translation>Nouveau message</translation>
    </message>
    <message id="whisperfish-session-section-today">
        <location filename="../qml/pages/Main.qml" line="117"/>
        <source>Today</source>
        <extracomment>Session section label for today</extracomment>
        <translation>Aujourd&apos;hui</translation>
    </message>
    <message id="whisperfish-session-section-yesterday">
        <location filename="../qml/pages/Main.qml" line="122"/>
        <source>Yesterday</source>
        <extracomment>Session section label for yesterday</extracomment>
        <translation>Hier</translation>
    </message>
    <message id="whisperfish-session-section-older">
        <location filename="../qml/pages/Main.qml" line="127"/>
        <source>Older</source>
        <extracomment>Session section label for older</extracomment>
        <translation>Plus ancien</translation>
    </message>
    <message id="whisperfish-about">
        <location filename="../qml/pages/About.qml" line="20"/>
        <source>About Whisperfish</source>
        <extracomment>Title for about page</extracomment>
        <translation>À propos de Whisperfish</translation>
    </message>
    <message id="whisperfish-version">
        <location filename="../qml/pages/About.qml" line="33"/>
        <source>Whisperfish v%1</source>
        <extracomment>Whisperfish version string</extracomment>
        <translation>Whisperfish v%1</translation>
    </message>
    <message id="whisperfish-description">
        <location filename="../qml/pages/About.qml" line="43"/>
        <source>Signal client for Sailfish OS</source>
        <extracomment>Whisperfish description</extracomment>
        <translation>Client Signal pour Sailfish OS</translation>
    </message>
    <message id="whisperfish-build-id">
        <location filename="../qml/pages/About.qml" line="54"/>
        <source>Build ID: %1</source>
        <extracomment>Whisperfish long version string and build ID</extracomment>
        <translation>ID version&#xa0;: %1</translation>
    </message>
    <message id="whisperfish-copyright">
        <location filename="../qml/pages/About.qml" line="64"/>
        <source>Copyright</source>
        <extracomment>Copyright</extracomment>
        <translation>Droit d&apos;auteur</translation>
    </message>
    <message id="whisperfish-liberapay">
        <location filename="../qml/pages/About.qml" line="79"/>
        <source>Support on Liberapay</source>
        <extracomment>Support on Liberapay</extracomment>
        <translation>Soutenir sur Liberapay</translation>
    </message>
    <message id="whisperfish-source-code">
        <location filename="../qml/pages/About.qml" line="89"/>
        <source>Source Code</source>
        <extracomment>Source Code</extracomment>
        <translation>Code source</translation>
    </message>
    <message id="whisperfish-bug-report">
        <location filename="../qml/pages/About.qml" line="99"/>
        <source>Report a Bug</source>
        <extracomment>Report a Bug</extracomment>
        <translation>Signaler une erreur</translation>
    </message>
    <message id="whisperfish-about-wiki-link">
        <location filename="../qml/pages/About.qml" line="109"/>
        <source>Visit the Wiki</source>
        <extracomment>Visit the Wiki button, tapping links to the Whisperfish Wiki</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-extra-copyright">
        <location filename="../qml/pages/About.qml" line="118"/>
        <source>Additional Copyright</source>
        <extracomment>Additional Copyright</extracomment>
        <translation>Droit d&apos;auteur additionnel</translation>
    </message>
    <message id="whisperfish-add-confirm">
        <location filename="../qml/pages/AddDevice.qml" line="25"/>
        <source>Add</source>
        <extracomment>&quot;Add&quot; message, shown in the link device dialog</extracomment>
        <translation>Ajouter</translation>
    </message>
    <message id="whisperfish-add-device">
        <location filename="../qml/pages/AddDevice.qml" line="33"/>
        <source>Add Device</source>
        <extracomment>Add Device, shown as pull-down menu item</extracomment>
        <translation>Ajouter un appareil</translation>
    </message>
    <message id="whisperfish-device-url">
        <location filename="../qml/pages/AddDevice.qml" line="43"/>
        <source>Device URL</source>
        <extracomment>Device URL, text input for pasting the QR-scanned code</extracomment>
        <translation>URL du périphérique</translation>
    </message>
    <message id="whisperfish-device-link-instructions">
        <location filename="../qml/pages/AddDevice.qml" line="54"/>
        <source>Install Signal Desktop. Use the CodeReader application to scan the QR code displayed on Signal Desktop and copy and paste the URL here.</source>
        <extracomment>Instructions on how to scan QR code for device linking</extracomment>
        <translation>Veuillez installer Signal Desktop et scanner le code QR avec l&apos;application CodeReader. Copiez-collez l&apos;URL ici.</translation>
    </message>
    <message id="whisperfish-attachment-from-self">
        <location filename="../qml/pages/AttachmentPage.qml" line="25"/>
        <location filename="../qml/pages/VideoAttachment.qml" line="24"/>
        <source>Me</source>
        <extracomment>Personalized placeholder showing the attachment is from oneself</extracomment>
        <translation>Moi</translation>
    </message>
    <message id="whisperfish-attachment-from-contact">
        <location filename="../qml/pages/AttachmentPage.qml" line="28"/>
        <location filename="../qml/pages/VideoAttachment.qml" line="27"/>
        <source>From %1</source>
        <extracomment>Personalized placeholder showing the attachment is from contact</extracomment>
        <translation>De %1</translation>
    </message>
    <message id="whisperfish-chatinput-contact">
        <location filename="../qml/pages/WFChatTextInput.qml" line="112"/>
        <source>Hi %1</source>
        <extracomment>Personalized placeholder for chat input, e.g. &quot;Hi John&quot;</extracomment>
        <translation>Salut %1</translation>
    </message>
    <message id="whisperfish-chatinput-generic">
        <location filename="../qml/pages/WFChatTextInput.qml" line="115"/>
        <source>Hi</source>
        <extracomment>Generic placeholder for chat input</extracomment>
        <translation>Salut</translation>
    </message>
    <message id="whisperfish-select-file">
        <location filename="../qml/pages/WFChatTextInput.qml" line="203"/>
        <source>Select file</source>
        <extracomment>Title for file picker page</extracomment>
        <translation>Choisir un fichier</translation>
    </message>
    <message id="whisperfish-choose-country-code">
        <location filename="../qml/pages/CountryCodeDialog.qml" line="17"/>
        <source>Choose Country Code</source>
        <extracomment>Directions for choosing country code</extracomment>
        <translation>Choisir un code pays</translation>
    </message>
    <message id="whisperfish-phone-number-input-label">
        <location filename="../qml/pages/Register.qml" line="52"/>
        <source>International phone number</source>
        <extracomment>Phone number input</extracomment>
        <translation>Numéro de téléphone international</translation>
    </message>
    <message id="whisperfish-phone-number-input-placeholder">
        <location filename="../qml/pages/Register.qml" line="55"/>
        <source>+18875550100</source>
        <extracomment>Phone number placeholder</extracomment>
        <translation>+33601234567</translation>
    </message>
    <message id="whisperfish-group-add-member-menu">
        <location filename="../qml/pages/Group.qml" line="18"/>
        <source>Add Member</source>
        <extracomment>Add group member menu item</extracomment>
        <translation>Ajouter un membre</translation>
    </message>
    <message id="whisperfish-group-add-member-remorse">
        <location filename="../qml/pages/Group.qml" line="25"/>
        <source>Adding %1 to group</source>
        <extracomment>Add group member remorse message</extracomment>
        <translation>%1 en cours d&apos;ajout au groupe</translation>
    </message>
    <message id="whisperfish-group-leave-menu">
        <location filename="../qml/pages/Group.qml" line="35"/>
        <source>Leave</source>
        <extracomment>Leave group menu item</extracomment>
        <translation>Quitter</translation>
    </message>
    <message id="whisperfish-group-leave-remorse">
        <location filename="../qml/pages/Group.qml" line="39"/>
        <source>Leaving group and removing ALL messages!</source>
        <extracomment>Leave group remorse message</extracomment>
        <translation>Quitter le groupe et suppression de TOUS les messages&#xa0;!</translation>
    </message>
    <message id="whisperfish-group-members-title">
        <location filename="../qml/pages/Group.qml" line="62"/>
        <source>Group members</source>
        <extracomment>Group members</extracomment>
        <translation>Membres du groupe</translation>
    </message>
    <message id="whisperfish-select-picture">
        <location filename="../qml/pages/ImagePicker.qml" line="44"/>
        <source>Select picture</source>
        <extracomment>Title for image picker page</extracomment>
        <translation>Sélectionner une photo</translation>
    </message>
    <message id="whisperfish-add-linked-device">
        <location filename="../qml/pages/LinkedDevices.qml" line="17"/>
        <source>Add</source>
        <extracomment>Menu option to add new linked device</extracomment>
        <translation>Ajouter</translation>
    </message>
    <message id="whisperfish-refresh-linked-devices">
        <location filename="../qml/pages/LinkedDevices.qml" line="30"/>
        <source>Refresh</source>
        <extracomment>Menu option to refresh linked devices</extracomment>
        <translation>Rafraîchir</translation>
    </message>
    <message id="whisperfish-linked-devices">
        <location filename="../qml/pages/LinkedDevices.qml" line="39"/>
        <source>Linked Devices</source>
        <extracomment>Title for Linked Devices page</extracomment>
        <translation>Appareils liés</translation>
    </message>
    <message id="whisperfish-device-unlink-message">
        <location filename="../qml/pages/LinkedDevices.qml" line="49"/>
        <source>Unlinking</source>
        <extracomment>Unlinking remorse info message for unlinking secondary devices.</extracomment>
        <translation>Lien en cours de suppression</translation>
    </message>
    <message id="whisperfish-current-device-name">
        <location filename="../qml/pages/LinkedDevices.qml" line="65"/>
        <source>Current device (Whisperfish, %1)</source>
        <extracomment>Linked device title for current Whisperfish</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-device-name">
        <location filename="../qml/pages/LinkedDevices.qml" line="69"/>
        <source>Device %1</source>
        <extracomment>Linked device name</extracomment>
        <translation>Appareil %1</translation>
    </message>
    <message id="whisperfish-device-link-date">
        <location filename="../qml/pages/LinkedDevices.qml" line="81"/>
        <source>Linked: %1</source>
        <extracomment>Linked device date</extracomment>
        <translation>Lié le&#xa0;: %1</translation>
    </message>
    <message id="whisperfish-device-last-active">
        <location filename="../qml/pages/LinkedDevices.qml" line="98"/>
        <source>Last active: %1</source>
        <extracomment>Linked device last active date</extracomment>
        <translation>Dernière activité&#xa0;: %1</translation>
    </message>
    <message id="whisperfish-device-unlink">
        <location filename="../qml/pages/LinkedDevices.qml" line="118"/>
        <source>Unlink</source>
        <extracomment>Device unlink menu option</extracomment>
        <translation>Supprimer le lien</translation>
    </message>
    <message id="whisperfish-registration-complete">
        <location filename="../qml/pages/Main.qml" line="27"/>
        <source>Registration complete!</source>
        <extracomment>Registration complete remorse message</extracomment>
        <translation>Enregistrement terminé&#xa0;!</translation>
    </message>
    <message id="whisperfish-error-invalid-datastore">
        <location filename="../qml/pages/Main.qml" line="32"/>
        <source>ERROR - Failed to setup datastore</source>
        <extracomment>Failed to setup datastore error message</extracomment>
        <translation>ERREUR – Échec de la configuration de la base de données</translation>
    </message>
    <message id="whisperfish-error-invalid-number">
        <location filename="../qml/pages/Main.qml" line="37"/>
        <source>ERROR - Invalid phone number registered with Signal</source>
        <extracomment>Invalid phone number error message</extracomment>
        <translation>ERREUR – Numéro de téléphone enregistré avec Signal invalide</translation>
    </message>
    <message id="whisperfish-error-setup-client">
        <location filename="../qml/pages/Main.qml" line="42"/>
        <source>ERROR - Failed to setup Signal client</source>
        <extracomment>Failed to setup signal client error message</extracomment>
        <translation>ERREUR – Échec de la configuration du client Signal</translation>
    </message>
    <message id="whisperfish-about-menu">
        <location filename="../qml/pages/Main.qml" line="58"/>
        <source>About Whisperfish</source>
        <extracomment>About whisperfish menu item</extracomment>
        <translation>À propos de Whisperfish</translation>
    </message>
    <message id="whisperfish-settings-menu">
        <location filename="../qml/pages/Main.qml" line="64"/>
        <source>Settings</source>
        <extracomment>Whisperfish settings menu item</extracomment>
        <translation>Paramètres</translation>
    </message>
    <message id="whisperfish-new-group-menu">
        <location filename="../qml/pages/Main.qml" line="71"/>
        <source>New Group</source>
        <extracomment>Whisperfish new group menu item</extracomment>
        <translation>Nouveau groupe</translation>
    </message>
    <message id="whisperfish-new-message-menu">
        <location filename="../qml/pages/Main.qml" line="78"/>
        <source>New Message</source>
        <extracomment>Whisperfish new message menu item</extracomment>
        <translation>Nouveau message</translation>
    </message>
    <message id="whisperfish-no-messages-found">
        <location filename="../qml/pages/Main.qml" line="92"/>
        <source>No messages</source>
        <extracomment>Whisperfish no messages found message</extracomment>
        <translation>Aucun message</translation>
    </message>
    <message id="whisperfish-registration-required-message">
        <location filename="../qml/pages/Main.qml" line="97"/>
        <source>Registration required</source>
        <extracomment>Whisperfish registration required message</extracomment>
        <translation>Enregistrement requis</translation>
    </message>
    <message id="whisperfish-locked-message">
        <location filename="../qml/pages/Main.qml" line="101"/>
        <source>Locked</source>
        <extracomment>Whisperfish locked message</extracomment>
        <translation>Verrouillé</translation>
    </message>
    <message id="whisperfish-group-label">
        <location filename="../qml/pages/MessagesView.qml" line="89"/>
        <source>Group: %1</source>
        <extracomment>Group message label</extracomment>
        <translation>Groupe&#xa0;: %1</translation>
    </message>
    <message id="whisperfish-delete-message">
        <location filename="../qml/pages/MessagesView.qml" line="102"/>
        <source>Deleting</source>
        <extracomment>Deleting message remorse</extracomment>
        <translation>Suppression en cours</translation>
    </message>
    <message id="whisperfish-resend-message">
        <location filename="../qml/pages/MessagesView.qml" line="112"/>
        <source>Resending</source>
        <extracomment>Resend message remorse</extracomment>
        <translation>Renvoi en cours</translation>
    </message>
    <message id="whisperfish-copy-message-menu">
        <location filename="../qml/pages/MessagesView.qml" line="135"/>
        <source>Copy</source>
        <extracomment>Copy message menu item</extracomment>
        <translation>Copier</translation>
    </message>
    <message id="whisperfish-open-message-menu">
        <location filename="../qml/pages/MessagesView.qml" line="141"/>
        <source>Open</source>
        <extracomment>Open attachment message menu item</extracomment>
        <translation>Ouvrir</translation>
    </message>
    <message id="whisperfish-delete-message-menu">
        <location filename="../qml/pages/MessagesView.qml" line="148"/>
        <source>Delete</source>
        <extracomment>Delete message menu item</extracomment>
        <translation>Supprimer</translation>
    </message>
    <message id="whisperfish-resend-message-menu">
        <location filename="../qml/pages/MessagesView.qml" line="154"/>
        <source>Resend</source>
        <extracomment>Resend message menu item</extracomment>
        <translation>Renvoyer</translation>
    </message>
    <message id="whisperfish-new-group-title">
        <location filename="../qml/pages/NewGroup.qml" line="37"/>
        <source>New Group</source>
        <extracomment>New group page title</extracomment>
        <translation>Nouveau groupe</translation>
    </message>
    <message id="whisperfish-group-name-label">
        <location filename="../qml/pages/NewGroup.qml" line="46"/>
        <source>Group Name</source>
        <extracomment>Group name label</extracomment>
        <translation>Nom du groupe</translation>
    </message>
    <message id="whisperfish-group-name-placeholder">
        <location filename="../qml/pages/NewGroup.qml" line="49"/>
        <source>Group Name</source>
        <extracomment>Group name placeholder</extracomment>
        <translation>Nom du groupe</translation>
    </message>
    <message id="whisperfish-new-group-message-members">
        <location filename="../qml/pages/NewGroup.qml" line="68"/>
        <location filename="../qml/pages/NewGroup.qml" line="72"/>
        <source>Members</source>
        <extracomment>New group message members label
----------
Summary of all selected recipients, e.g. &quot;Bob, Jane, 75553243&quot;</extracomment>
        <translation>Membres</translation>
    </message>
    <message id="whisperfish-error-invalid-group-members">
        <location filename="../qml/pages/NewGroup.qml" line="100"/>
        <source>Please select group members</source>
        <extracomment>Invalid recipient error</extracomment>
        <translation>Sélectionner membres du groupe</translation>
    </message>
    <message id="whisperfish-error-invalid-group-name">
        <location filename="../qml/pages/NewGroup.qml" line="104"/>
        <source>Please name the group</source>
        <extracomment>Invalid group name error</extracomment>
        <translation>Nommer le groupe</translation>
    </message>
    <message id="whisperfish-recipient-number-invalid-chars">
        <location filename="../qml/pages/NewMessage.qml" line="53"/>
        <source>This phone number contains invalid characters.</source>
        <extracomment>invalid recipient phone number: invalid characters</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-recipient-local-number-not-allowed">
        <location filename="../qml/pages/NewMessage.qml" line="58"/>
        <source>Please set a country code in the settings, or use the international format.</source>
        <extracomment>invalid recipient phone number: local numbers are not allowed</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-recipient-number-invalid-unspecified">
        <location filename="../qml/pages/NewMessage.qml" line="62"/>
        <source>This phone number appears to be invalid.</source>
        <extracomment>invalid recipient phone number: failed to format</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-new-message-title">
        <location filename="../qml/pages/NewMessage.qml" line="88"/>
        <source>New message</source>
        <extracomment>New message page title</extracomment>
        <translation>Nouveau message</translation>
    </message>
    <message id="whisperfish-new-message-recipient">
        <location filename="../qml/pages/NewMessage.qml" line="106"/>
        <location filename="../qml/pages/NewMessage.qml" line="110"/>
        <source>Recipient</source>
        <extracomment>New message recipient label
----------
Summary of all selected recipients, e.g. &quot;Bob, Jane, 75553243&quot;</extracomment>
        <translation>Destinataire</translation>
    </message>
    <message id="whisperfish-error-invalid-recipient">
        <location filename="../qml/pages/NewMessage.qml" line="152"/>
        <source>Invalid recipient</source>
        <extracomment>Invalid recipient error</extracomment>
        <translation>Destinataire invalide</translation>
    </message>
    <message id="whisperfish-enter-password">
        <location filename="../qml/pages/Password.qml" line="45"/>
        <source>Enter your password</source>
        <extracomment>Enter password prompt</extracomment>
        <translation>Entrez votre mot de passe</translation>
    </message>
    <message id="whisperfish-set-password">
        <location filename="../qml/pages/Password.qml" line="48"/>
        <source>Set your password</source>
        <extracomment>Set password prompt</extracomment>
        <translation>Définissez votre mot de passe</translation>
    </message>
    <message id="whisperfish-password-label">
        <location filename="../qml/pages/Password.qml" line="58"/>
        <source>Password</source>
        <extracomment>Password label</extracomment>
        <translation>Mot de passe</translation>
    </message>
    <message id="whisperfish-password-placeholder">
        <location filename="../qml/pages/Password.qml" line="61"/>
        <source>Password</source>
        <extracomment>Password placeholder</extracomment>
        <translation>Mot de passe</translation>
    </message>
    <message id="whisperfish-verify-password-label">
        <location filename="../qml/pages/Password.qml" line="83"/>
        <source>Verify Password</source>
        <extracomment>Verify Password label</extracomment>
        <translation>Vérifier le mot de passe</translation>
    </message>
    <message id="whisperfish-verify-password-placeholder">
        <location filename="../qml/pages/Password.qml" line="86"/>
        <source>Verify Password</source>
        <extracomment>Verify Password label</extracomment>
        <translation>Vérifier le mot de passe</translation>
    </message>
    <message id="whisperfish-password-info">
        <location filename="../qml/pages/Password.qml" line="104"/>
        <source>Whisperfish stores identity keys, session state, and local message data encrypted on disk. The password you set is not stored anywhere and you will not be able to restore your data if you lose your password. Note: Attachments are currently stored unencrypted. You can disable storing of attachments in the Settings page.</source>
        <extracomment>Whisperfish password informational message</extracomment>
        <translation>Whisperfish stocke les clés d&apos;identité, l&apos;état de la session et les données de message local chiffrées sur disque. Le mot de passe que vous avez défini n&apos;est stocké nulle part et vous ne pourrez pas restaurer vos données si vous perdez votre mot de passe. Remarque&#xa0;: les pièces jointes sont actuellement stockées non chiffrées. Vous pouvez désactiver le stockage des pièces jointes dans la page Paramètres.</translation>
    </message>
    <message id="whisperfish-reset-peer-accept">
        <location filename="../qml/pages/PeerIdentityChanged.qml" line="24"/>
        <location filename="../qml/pages/ResetPeerIdentity.qml" line="24"/>
        <source>Confirm</source>
        <extracomment>Reset peer identity accept text</extracomment>
        <translation>Confirmer</translation>
    </message>
    <message id="whisperfish-peer-not-trusted">
        <location filename="../qml/pages/PeerIdentityChanged.qml" line="32"/>
        <location filename="../qml/pages/ResetPeerIdentity.qml" line="32"/>
        <source>Peer identity is not trusted</source>
        <extracomment>Peer identity not trusted</extracomment>
        <translation>L&apos;identité de l&apos;interlocuteur est corrompue</translation>
    </message>
    <message id="whisperfish-peer-not-trusted-message">
        <location filename="../qml/pages/PeerIdentityChanged.qml" line="42"/>
        <location filename="../qml/pages/ResetPeerIdentity.qml" line="42"/>
        <source>WARNING: %1 identity is no longer trusted. Tap Confirm to reset peer identity.</source>
        <extracomment>Peer identity not trusted message</extracomment>
        <translation>ATTENTION&#xa0;: identité %1 corrompue. Appuyez sur Confirmer pour réinitialiser l&apos;identité de l&apos;interlocuteur.</translation>
    </message>
    <message id="whisperfish-register-accept">
        <location filename="../qml/pages/Register.qml" line="25"/>
        <source>Register</source>
        <extracomment>Register accept text</extracomment>
        <translation>S&apos;enregistrer</translation>
    </message>
    <message id="whisperfish-registration-message">
        <location filename="../qml/pages/Register.qml" line="41"/>
        <source>Enter the phone number you want to register with Signal.</source>
        <extracomment>Registration message</extracomment>
        <translation>Saisissez le numéro de téléphone que vous voulez enregistrer avec Signal.</translation>
    </message>
    <message id="whisperfish-share-contacts-label">
        <location filename="../qml/pages/Register.qml" line="66"/>
        <location filename="../qml/pages/Settings.qml" line="191"/>
        <source>Share Contacts</source>
        <extracomment>Share contacts label
----------
Settings page share contacts</extracomment>
        <translation>Partager des contacts</translation>
    </message>
    <message id="whisperfish-share-contacts-description">
        <location filename="../qml/pages/Register.qml" line="69"/>
        <location filename="../qml/pages/Settings.qml" line="194"/>
        <source>Allow Signal to use your local contact list, to find other Signal users.</source>
        <extracomment>Share contacts description</extracomment>
        <translation>Autorisez Signal à utiliser votre liste de contacts locale pour trouver d&apos;autres utilisateurs de Signal.</translation>
    </message>
    <message id="whisperfish-verification-method-label">
        <location filename="../qml/pages/Register.qml" line="87"/>
        <source>Verification method</source>
        <extracomment>Verification method</extracomment>
        <translation>Méthode de vérification</translation>
    </message>
    <message id="whisperfish-use-text-verification">
        <location filename="../qml/pages/Register.qml" line="100"/>
        <source>Use text verification</source>
        <extracomment>Text verification</extracomment>
        <translation>Utiliser la vérification par SMS</translation>
    </message>
    <message id="whisperfish-use-voice-verification">
        <location filename="../qml/pages/Register.qml" line="105"/>
        <source>Use voice verification</source>
        <extracomment>Voice verification</extracomment>
        <translation>Utiliser la vérification vocale</translation>
    </message>
    <message id="whisperfish-voice-registration-directions">
        <location filename="../qml/pages/Register.qml" line="92"/>
        <source>Signal will call you with a 6-digit verification code. Please be ready to write this down.</source>
        <extracomment>Registration directions</extracomment>
        <translation>Signal vous appellera avec un code de vérification à 6 chiffres. Soyez prêt·e pour le noter.</translation>
    </message>
    <message id="whisperfish-text-registration-directions">
        <location filename="../qml/pages/Register.qml" line="94"/>
        <source>Signal will text you a 6-digit verification code.</source>
        <translation>Signal vous enverra un code de vérification à 6 chiffres par SMS.</translation>
    </message>
    <message id="whisperfish-settings-linked-devices-menu">
        <location filename="../qml/pages/Settings.qml" line="25"/>
        <source>Linked Devices</source>
        <extracomment>Linked devices menu option</extracomment>
        <translation>Appareils liés</translation>
    </message>
    <message id="whisperfish-settings-reconnect-menu">
        <location filename="../qml/pages/Settings.qml" line="34"/>
        <source>Reconnect</source>
        <extracomment>Reconnect menu</extracomment>
        <translation>Reconnecter</translation>
    </message>
    <message id="whisperfish-settings-refresh-contacts-menu">
        <location filename="../qml/pages/Settings.qml" line="42"/>
        <source>Refresh Contacts</source>
        <extracomment>Refresh contacts menu</extracomment>
        <translation>Actualiser les contacts</translation>
    </message>
    <message id="whisperfish-settings-title">
        <location filename="../qml/pages/Settings.qml" line="59"/>
        <source>Whisperfish Settings</source>
        <extracomment>Settings page title</extracomment>
        <translation>Paramètres de Whisperfish</translation>
    </message>
    <message id="whisperfish-settings-identity-section-label">
        <location filename="../qml/pages/Settings.qml" line="66"/>
        <source>My Identity</source>
        <extracomment>Settings page My identity section label</extracomment>
        <translation>Mon identité</translation>
    </message>
    <message id="whisperfish-settings-my-phone-number">
        <location filename="../qml/pages/Settings.qml" line="75"/>
        <source>My Phone</source>
        <oldsource>Phone</oldsource>
        <extracomment>Settings page My phone number</extracomment>
        <translation>Mon num. téléphone</translation>
    </message>
    <message id="whisperfish-settings-my-uuid">
        <location filename="../qml/pages/Settings.qml" line="85"/>
        <source>My UUID registration number</source>
        <extracomment>Settings page My UUID</extracomment>
        <translation>Mon numéro d&apos;enregistrement UUID</translation>
    </message>
    <message id="whisperfish-settings-identity-label">
        <location filename="../qml/pages/Settings.qml" line="96"/>
        <source>Identity</source>
        <extracomment>Settings page Identity label</extracomment>
        <translation>Identité</translation>
    </message>
    <message id="whisperfish-settings-notifications-section">
        <location filename="../qml/pages/Settings.qml" line="105"/>
        <source>Notifications</source>
        <extracomment>Settings page notifications section</extracomment>
        <translation>Notifications</translation>
    </message>
    <message id="whisperfish-settings-notifications-enable">
        <location filename="../qml/pages/Settings.qml" line="112"/>
        <source>Enable notifications</source>
        <oldsource>Enabled</oldsource>
        <extracomment>Settings page notifications enable</extracomment>
        <translation>Activer les notifications</translation>
    </message>
    <message id="whisperfish-settings-notifications-enable-description">
        <location filename="../qml/pages/Settings.qml" line="115"/>
        <source>If turned off, Whisperfish will not send any notification</source>
        <extracomment>Settings page notifications enable description</extracomment>
        <translation>Si ceci est désactivé, Whisperfish n&apos;enverra aucune notification</translation>
    </message>
    <message id="whisperfish-settings-notifications-show-body">
        <location filename="../qml/pages/Settings.qml" line="128"/>
        <source>Show Message Body</source>
        <extracomment>Settings page notifications show message body</extracomment>
        <translation>Afficher le corps du message</translation>
    </message>
    <message id="whisperfish-settings-notifications-show-body-description">
        <location filename="../qml/pages/Settings.qml" line="131"/>
        <source>If turned off, Whisperfish will only show the sender of a message, not the contents.</source>
        <extracomment>Settings page notifications show message body description</extracomment>
        <translation>Si ceci est désactivé, Whisperfish affichera uniquement l&apos;expéditeur d&apos;un message, pas le contenu.</translation>
    </message>
    <message id="whisperfish-settings-general-section">
        <location filename="../qml/pages/Settings.qml" line="146"/>
        <source>General</source>
        <extracomment>Settings page general section</extracomment>
        <translation>Général</translation>
    </message>
    <message id="whisperfish-settings-country-code">
        <location filename="../qml/pages/Settings.qml" line="153"/>
        <source>Country Code</source>
        <extracomment>Settings page country code</extracomment>
        <translation>Code pays</translation>
    </message>
    <message id="whisperfish-settings-country-code-description">
        <location filename="../qml/pages/Settings.qml" line="156"/>
        <source>The selected country code determines what happens when a local phone number is entered.</source>
        <extracomment>Settings page country code description</extracomment>
        <translation>Le code pays sélectionné détermine ce qui se passe quand un numéro de téléphone local est saisi.</translation>
    </message>
    <message id="whisperfish-settings-save-attachments">
        <location filename="../qml/pages/Settings.qml" line="171"/>
        <source>Save Attachments</source>
        <extracomment>Settings page save attachments</extracomment>
        <translation>Sauvegarder les pièces jointes</translation>
    </message>
    <message id="whisperfish-settings-save-attachments-description">
        <location filename="../qml/pages/Settings.qml" line="175"/>
        <source>Attachments are stored at %1</source>
        <extracomment>Settings page save attachments description</extracomment>
        <translation>Les pièces jointes sont stockées dans %1</translation>
    </message>
    <message id="whisperfish-settings-enable-enter-send">
        <location filename="../qml/pages/Settings.qml" line="208"/>
        <source>Return key send</source>
        <oldsource>EnterKey Send</oldsource>
        <extracomment>Settings page enable enter send</extracomment>
        <translation>Envoi par la touche de retour</translation>
    </message>
    <message id="whisperfish-settings-enable-enter-send-description">
        <location filename="../qml/pages/Settings.qml" line="211"/>
        <source>When enabled, the return key functions as a send key. Otherwise, the return key can be used for multi-line messages.</source>
        <extracomment>Settings page enable enter send description</extracomment>
        <translation>Quand ceci est activé, la touche de retour fonctionne comme une touche d&apos;envoi. Sinon, la touche de retour peut être utilisée pour les messages multilignes.</translation>
    </message>
    <message id="whisperfish-settings-advanced-section">
        <location filename="../qml/pages/Settings.qml" line="226"/>
        <source>Advanced</source>
        <extracomment>Settings page advanced section</extracomment>
        <translation>Avancés</translation>
    </message>
    <message id="whisperfish-settings-incognito-mode">
        <location filename="../qml/pages/Settings.qml" line="233"/>
        <source>Incognito Mode</source>
        <extracomment>Settings page incognito mode</extracomment>
        <translation>Mode incognito</translation>
    </message>
    <message id="whisperfish-settings-incognito-mode-description">
        <location filename="../qml/pages/Settings.qml" line="236"/>
        <source>Incognito Mode disables storage entirely. No attachments nor messages are saved, messages are visible until restart.</source>
        <extracomment>Settings page incognito mode description</extracomment>
        <translation>Le mode Incognito désactive entièrement le stockage. Aucune pièce jointe ni aucun message n&apos;est enregistré, les messages sont visibles jusqu&apos;au redémarrage.</translation>
    </message>
    <message id="whisperfish-settings-restarting-message">
        <location filename="../qml/pages/Settings.qml" line="244"/>
        <source>Restart Whisperfish...</source>
        <extracomment>Restart whisperfish message</extracomment>
        <translation>Redémarrer Whisperfish…</translation>
    </message>
    <message id="whisperfish-settings-scale-image-attachments">
        <location filename="../qml/pages/Settings.qml" line="257"/>
        <source>Scale JPEG Attachments</source>
        <extracomment>Settings page scale image attachments</extracomment>
        <translation>Redimensionner annexes JPEG</translation>
    </message>
    <message id="whisperfish-settings-scale-image-attachments-description">
        <location filename="../qml/pages/Settings.qml" line="260"/>
        <source>Scale down JPEG attachments to save on bandwidth.</source>
        <extracomment>Settings page scale image attachments description</extracomment>
        <translation>Réduire les pièces jointes JPEG pour économiser de la bande passante.</translation>
    </message>
    <message id="whisperfish-settings-debug-mode">
        <location filename="../qml/pages/Settings.qml" line="274"/>
        <source>Debug mode</source>
        <extracomment>Settings page: debug info toggle</extracomment>
        <translation>Mode débogage</translation>
    </message>
    <message id="whisperfish-settings-debug-mode-description">
        <location filename="../qml/pages/Settings.qml" line="277"/>
        <source>Show debugging information in the user interface.</source>
        <extracomment>Settings page: debug info toggle extended description</extracomment>
        <translation>Afficher les informations de débogage dans l&apos;interface utilisateur.</translation>
    </message>
    <message id="whisperfish-settings-stats-section">
        <location filename="../qml/pages/Settings.qml" line="292"/>
        <source>Statistics</source>
        <extracomment>Settings page stats section</extracomment>
        <translation>Statistiques</translation>
    </message>
    <message id="whisperfish-settings-websocket">
        <location filename="../qml/pages/Settings.qml" line="297"/>
        <source>Websocket Status</source>
        <extracomment>Settings page websocket status</extracomment>
        <translation>Statut du websocket</translation>
    </message>
    <message id="whisperfish-settings-connected">
        <location filename="../qml/pages/Settings.qml" line="301"/>
        <source>Connected</source>
        <extracomment>Settings page connected message</extracomment>
        <translation>Connecté</translation>
    </message>
    <message id="whisperfish-settings-disconnected">
        <location filename="../qml/pages/Settings.qml" line="304"/>
        <source>Disconnected</source>
        <extracomment>Settings page disconnected message</extracomment>
        <translation>Déconnecté</translation>
    </message>
    <message id="whisperfish-settings-unsent-messages">
        <location filename="../qml/pages/Settings.qml" line="309"/>
        <source>Unsent Messages</source>
        <extracomment>Settings page unsent messages</extracomment>
        <translation>Messages non envoyés</translation>
    </message>
    <message id="whisperfish-settings-total-sessions">
        <location filename="../qml/pages/Settings.qml" line="315"/>
        <source>Total Sessions</source>
        <extracomment>Settings page total sessions</extracomment>
        <translation>Total des sessions</translation>
    </message>
    <message id="whisperfish-settings-total-messages">
        <location filename="../qml/pages/Settings.qml" line="321"/>
        <source>Total Messages</source>
        <extracomment>Settings page total messages</extracomment>
        <translation>Total des messages</translation>
    </message>
    <message id="whisperfish-settings-total-contacts">
        <location filename="../qml/pages/Settings.qml" line="327"/>
        <source>Signal Contacts</source>
        <extracomment>Settings page total signal contacts</extracomment>
        <translation>Contacts Signal</translation>
    </message>
    <message id="whisperfish-settings-encrypted-keystore">
        <location filename="../qml/pages/Settings.qml" line="333"/>
        <source>Encrypted Key Store</source>
        <extracomment>Settings page encrypted key store</extracomment>
        <translation>Stockage de clés chiffrées</translation>
    </message>
    <message id="whisperfish-settings-encrypted-keystore-enabled">
        <location filename="../qml/pages/Settings.qml" line="337"/>
        <source>Enabled</source>
        <extracomment>Settings page encrypted key store enabled</extracomment>
        <translation>Activé</translation>
    </message>
    <message id="whisperfish-settings-encrypted-keystore-disabled">
        <location filename="../qml/pages/Settings.qml" line="340"/>
        <source>Disabled</source>
        <extracomment>Settings page encrypted key store disabled</extracomment>
        <translation>Desactivé</translation>
    </message>
    <message id="whisperfish-settings-encrypted-db">
        <location filename="../qml/pages/Settings.qml" line="345"/>
        <source>Encrypted Database</source>
        <extracomment>Settings page encrypted database</extracomment>
        <translation>Base de données chiffrée</translation>
    </message>
    <message id="whisperfish-settings-encrypted-db-enabled">
        <location filename="../qml/pages/Settings.qml" line="349"/>
        <source>Enabled</source>
        <extracomment>Settings page encrypted db enabled</extracomment>
        <translation>Activé</translation>
    </message>
    <message id="whisperfish-settings-encrypted-db-disabled">
        <location filename="../qml/pages/Settings.qml" line="352"/>
        <source>Disabled</source>
        <extracomment>Settings page encrypted db disabled</extracomment>
        <translation>Désactivé</translation>
    </message>
    <message id="whisperfish-verify-code-accept">
        <location filename="../qml/pages/Verify.qml" line="25"/>
        <source>Verify</source>
        <extracomment>Verify code accept</extracomment>
        <translation>Vérifier</translation>
    </message>
    <message id="whisperfish-verify-code-title">
        <location filename="../qml/pages/Verify.qml" line="33"/>
        <source>Verify Device</source>
        <extracomment>Verify code page title</extracomment>
        <translation>Vérifier l&apos;appareil</translation>
    </message>
    <message id="whisperfish-verify-code-label">
        <location filename="../qml/pages/Verify.qml" line="43"/>
        <source>Code</source>
        <extracomment>Verify code label</extracomment>
        <translation>Code</translation>
    </message>
    <message id="whisperfish-verify-code-placeholder">
        <location filename="../qml/pages/Verify.qml" line="46"/>
        <source>123456</source>
        <extracomment>Verify code placeholder</extracomment>
        <translation>123456</translation>
    </message>
    <message id="whisperfish-voice-verify-code-instructions">
        <location filename="../qml/pages/Verify.qml" line="61"/>
        <source>Signal will call you with a 6-digit verification code. Please enter it here.</source>
        <extracomment>Voice verification code instructions</extracomment>
        <translation>Signal vous appellera avec un code de vérification à 6 chiffres. Veuillez les saisir ici.</translation>
    </message>
    <message id="whisperfish-text-verify-code-instructions">
        <location filename="../qml/pages/Verify.qml" line="72"/>
        <source>Signal will text you a 6-digit verification code. Please enter it here, using only numbers.</source>
        <extracomment>Text verification code instructions</extracomment>
        <translation>Signal vous enverra un code de vérification à 6 chiffres par SMS. Veuillez les saisir ici, uniquement des chiffres.</translation>
    </message>
    <message id="whisperfish-reset-session-menu">
        <location filename="../qml/pages/VerifyIdentity.qml" line="18"/>
        <source>Reset Secure Session</source>
        <extracomment>Reset secure session menu item</extracomment>
        <translation>Réinitialiser la session sécurisée</translation>
    </message>
    <message id="whisperfish-reset-session-message">
        <location filename="../qml/pages/VerifyIdentity.qml" line="23"/>
        <source>Resetting secure session</source>
        <extracomment>Reset secure session remorse message</extracomment>
        <translation>Réinitialisation de la session sécurisée</translation>
    </message>
    <message id="whisperfish-verify-contact-identity-title">
        <location filename="../qml/pages/VerifyIdentity.qml" line="44"/>
        <source>Verify safety numbers</source>
        <extracomment>Verify safety numbers</extracomment>
        <translation>Vérifier les numéros de sécurité</translation>
    </message>
    <message id="whisperfish-numeric-fingerprint-directions">
        <location filename="../qml/pages/VerifyIdentity.qml" line="63"/>
        <source>If you wish to verify the security of your end-to-end encryption with %1, compare the numbers above with the numbers on their device.</source>
        <extracomment>Numeric fingerprint instructions</extracomment>
        <translation>Si vous souhaitez vérifier la fiabilité de votre chiffrement de bout en bout avec %1, comparez les chiffres ci-dessus avec les chiffres sur votre appareil.</translation>
    </message>
</context>
</TS>
